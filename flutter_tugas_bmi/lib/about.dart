import 'package:flutter/cupertino.dart'; 
import 'package:flutter/material.dart';

class MyProfil extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
    new  Scaffold(
      appBar:  AppBar(
        backgroundColor: Colors.yellowAccent,foregroundColor:Colors.blue,
        title: Text('PROFIL',style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 50),),
        centerTitle: true,
      ),
      body:  ListView(
        children:  <Widget>[
          Container(
            color: Color(0xa6eee0aa),
            child: Stack(
              children: <Widget>[
                Padding(
                  padding:  EdgeInsets.only(top:  30),
                  child:  Align(
                    alignment:  Alignment.topCenter,
                    child:  Container(
                      height:  200,
                      width:  200,
                      decoration:  BoxDecoration(
                        borderRadius:  BorderRadius.circular(100),
                        image: DecorationImage(image: AssetImage('gambar/alit.jpg'),
                          fit:  BoxFit.cover
                        )
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

          Container(
            color:  Colors.white,
            child:  Column(
            children: <Widget>[
              SizedBox(height: 20),
              Text('KETUT ALIT WINGGASAPUTRA',style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 50),),

              SizedBox(height: 5,child: Container(
                color:  Colors.white,
              ),),
              Row(
                mainAxisAlignment:  MainAxisAlignment.center,
                children:  <Widget>[

                  Text('Universitas Pendidikan  Ganesha',style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 30),),
                ],
              ),
            ],
          ),
        ),
        Container(
          padding:  EdgeInsets.only(top:  30,  right:  50,  left:  50),
          child:  Column(
            children: <Widget>[
              Padding(
                padding:  const  EdgeInsets.all(0),
                child:  Row(
                  mainAxisAlignment:  MainAxisAlignment.spaceEvenly,
                  children:  <Widget>[
                    Container(
                      height:  100,
                      width:  100,
                      decoration: BoxDecoration(
                        border:  Border.all(color:  Colors.black),
                        //color: Colors.green,
                        borderRadius:  BorderRadius.only(topRight: Radius.circular(20),  topLeft:  Radius.circular(20)),
                      ),

                      child:  Padding(
                        padding:  const  EdgeInsets.all(12.0),
                        child:  Column(
                          children:  <Widget>[
                            Icon(Icons.school,
                              color:  Colors.lightGreenAccent,
                              size: 58),
                            Text('Undiksha',
                              style: TextStyle
                                (color: Colors.blue, fontWeight:  FontWeight.bold
                              ), )
                          ],
                        ),
                      ),
                    ),

                    Container(
                      height:  100,
                      width:  100,
                      decoration: BoxDecoration(
                        border:  Border.all(color:  Colors.blue),
                        //color: Colors.green,
                        borderRadius:  BorderRadius.only(topRight: Radius.circular(20),  topLeft:  Radius.circular(20)),
                      ),
                      child: Padding(
                        padding:  const  EdgeInsets.all(12.0),
                        child:  Column(
                          children:  <Widget>[
                            Icon(Icons.my_location,
                            color:  Colors.lightGreenAccent,
                            size: 58,),
                            Text('Singaraja', style: TextStyle
                              (color:  Colors.blue,
                                fontWeight: FontWeight.bold
                            ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding:  const  EdgeInsets.only(top:  40),
                child:  Row(
                  mainAxisAlignment:  MainAxisAlignment.spaceEvenly,
                  children:  <Widget>[
                    Container(
                      height:  100,
                      width:  100,
                      decoration: BoxDecoration(
                        border:  Border.all(color:  Colors.black),
                        //color: Colors.green ,
                        borderRadius:  BorderRadius.only(topRight: Radius.circular(20),  topLeft:  Radius.circular(20)),
                      ),
                      child: Padding(
                        padding:  const  EdgeInsets.all(12.0),
                        child:  Column(
                          children:  <Widget>[
                            Icon(Icons.food_bank,
                            color:  Colors.lightGreenAccent,
                            size: 58,),
                            Text('hotspicy',  style:  TextStyle
                              (color:  Colors.blue,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height:  100,
                    width:  100,
                    decoration: BoxDecoration(
                      border:  Border.all(color:  Colors.black),
                      borderRadius:  BorderRadius.only(topRight: Radius.circular(20),  topLeft:  Radius.circular(20)),
                    ),
                    child: Padding(
                      padding:  const  EdgeInsets.all(12.0),
                      child:  Column(
                        children:  <Widget>[
                          Icon(Icons.videogame_asset,
                          color:  Colors.lightGreenAccent,
                          size: 58,),
                          Text('RPG',style:  TextStyle
                          (color:  Colors.blue,
                            fontWeight: FontWeight.bold
                          ),
                          ),
                        ],
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        )
      ] ,
    ),
  );
}
}
